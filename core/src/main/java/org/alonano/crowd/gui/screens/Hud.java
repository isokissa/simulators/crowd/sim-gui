package org.alonano.crowd.gui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import org.alonano.crowd.gui.SimGui;

public class Hud implements Disposable {
    private Viewport viewport;
    private Stage stage;

    private Label zoomFactorLabel;

    public Hud() {
        viewport = new FitViewport(SimGui.V_WIDTH, SimGui.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport);

        zoomFactorLabel = new Label("kaka", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        Table table = new Table();
        table.top();
        table.setFillParent(true);
        table.add(zoomFactorLabel).expandX().padTop(20);

        stage.addActor(table);
    }


    @Override
    public void dispose() {
        stage.dispose();
    }

    public void update() {
        // TODO SOMETHING SMART
        stage.draw();
    }
}
