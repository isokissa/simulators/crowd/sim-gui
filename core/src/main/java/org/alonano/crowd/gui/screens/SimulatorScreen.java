package org.alonano.crowd.gui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import org.alonano.crowd.gui.SimGui;
import org.alonano.crowd.sim.Arena;

import java.util.stream.IntStream;

public class SimulatorScreen implements Screen, SimulatorScreenControl {
    private OrthographicCamera camera;
    private Viewport viewport;
    private ShapeRenderer shapeRenderer;
    private Hud hud;
    private ArenaRenderer arenaRenderer;
    private Arena arena;

    public SimulatorScreen(Arena arena, ArenaRenderer arenaRenderer) {
        this.arena = arena;
        this.arenaRenderer = arenaRenderer;
        camera  = new OrthographicCamera();
        camera.setToOrtho(false, SimGui.V_WIDTH, SimGui.V_HEIGHT);
        viewport = new ScreenViewport(camera);
        shapeRenderer = new ShapeRenderer();
        hud = new Hud();
        Gdx.input.setInputProcessor(new SimulatorInputProcessor(this));
    }

    @Override
    public void show() {

    }

    private void update(float delta) {
        arena.advance(delta);
        hud.update();
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(.1f, 0.1f, .1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        shapeRenderer.setProjectionMatrix(camera.combined);

        showGrid();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        arenaRenderer.renderArena(shapeRenderer, arena);
        shapeRenderer.end();
    }

    private void showGrid() {
        final int axisLength = 2000;
        final int marksEvery = 50;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.line(-axisLength, 0, axisLength, 0);
        shapeRenderer.line(0, -axisLength, 0, axisLength);
        IntStream.rangeClosed(-axisLength/marksEvery, axisLength/marksEvery).forEach(a -> {
            shapeRenderer.line(-3, a * marksEvery, +3, a * marksEvery);
            shapeRenderer.line(a * marksEvery, -3, a * marksEvery, +3);
        });
        shapeRenderer.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void move(float x, float y) {
        camera.translate(-x * camera.zoom, y * camera.zoom);
    }

    @Override
    public void zoom(float factor) {
        camera.zoom = camera.zoom * factor;
    }
}
