package org.alonano.crowd.gui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import org.alonano.crowd.gui.screens.ArenaRenderer;
import org.alonano.crowd.gui.screens.SimulatorScreen;
import org.alonano.crowd.sim.Arena;
import org.alonano.crowd.sim.spawners.PeriodicSpawner;

public class SimGui extends Game {
	public static final int V_WIDTH = 1024;
	public static final int V_HEIGHT = 768;

	@Override
	public void create () {
		Arena arena = createArena();
		setScreen(createSimulatorScreen(arena));
	}

	private Arena createArena() {
		Arena arena = new Arena();
		arena.add(new PeriodicSpawner(100, 150, .5f, 10, 10));
		arena.add(new PeriodicSpawner(1200, 550, .5f, 200, 10));
		return arena;
	}

	private SimulatorScreen createSimulatorScreen(Arena arena) {
		return new SimulatorScreen(arena, new ArenaRenderer());
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
	}
}
