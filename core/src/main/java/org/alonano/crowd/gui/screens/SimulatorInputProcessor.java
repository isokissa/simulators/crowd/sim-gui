package org.alonano.crowd.gui.screens;

import com.badlogic.gdx.InputProcessor;

public class SimulatorInputProcessor implements InputProcessor {
    private SimulatorScreenControl simulatorScreenControl;
    private float previousDragX;
    private float previousDragY;

    public SimulatorInputProcessor(SimulatorScreenControl simulatorScreenControl) {
        this.simulatorScreenControl = simulatorScreenControl;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        previousDragX = screenX;
        previousDragY = screenY;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        simulatorScreenControl.move(screenX - previousDragX, screenY - previousDragY);
        previousDragX = screenX;
        previousDragY = screenY;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        float factor = 1;
        if (amount < 0) {
            factor = .95f;
        } else if (amount > 0) {
            factor = 1.05f;
        }
        simulatorScreenControl.zoom(factor);
        return true;
    }
}
