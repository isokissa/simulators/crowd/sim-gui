package org.alonano.crowd.gui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import org.alonano.crowd.sim.Arena;
import org.alonano.crowd.sim.PlaneBody;

public class ArenaRenderer {
    private float pixelsPerMeter = 1;

    public void setPixelsPerMeter(float pixelsPerMeter) {
        this.pixelsPerMeter = pixelsPerMeter;
    }

    public float getPixelsPerMeter() {
        return this.pixelsPerMeter;
    }

    /**
     * Renders the given arena using the given spaheRenderer
     * @param shapeRenderer
     * @param arena
     */
    public void renderArena(ShapeRenderer shapeRenderer, Arena arena) {
        shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        arena.getAllBodies().stream().forEach(b -> renderBody(shapeRenderer, b, 3));
    }

    /**
     * Renders the body in its position.
     * @param body
     * @param radius radius of body's representation, in pixels.
     */
    public void renderBody(ShapeRenderer shapeRenderer, PlaneBody body, float radius) {
        final float triangleLength = radius * 3;
        final float triangleWidth = radius;
        float x = body.getX() - triangleLength / 2;
        float y = body.getY() - triangleWidth / 2;

        shapeRenderer.setColor(getBodyColor(body));

        shapeRenderer.identity();
        shapeRenderer.translate(x, y, 0);
        shapeRenderer.rotate(0, 0, 1, body.getAngle());
        shapeRenderer.translate(-radius, 0, 0);

        shapeRenderer.triangle(0, triangleWidth, 0, -triangleWidth, triangleLength, 0);

        shapeRenderer.translate(radius, 0, 0);
        shapeRenderer.rotate(0, 0, 1, -body.getAngle());
        shapeRenderer.translate(-x, -y, 0);
    }

    private Color getBodyColor(PlaneBody body) {
        switch (body.getState()) {
            case INVISIBLE: return Color.LIGHT_GRAY;
            case COLLIDED: return Color.RED;
            case NORMAL: return Color.YELLOW;
            default: return Color.NAVY;
        }
    }
}
