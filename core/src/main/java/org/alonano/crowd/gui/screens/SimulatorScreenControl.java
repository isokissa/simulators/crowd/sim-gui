package org.alonano.crowd.gui.screens;

public interface SimulatorScreenControl {

    /**
     * @param x
     * @param y
     */
    void move(float x, float y);

    /**
     * @param factor greater than 1 for zoom-in, otherwise zoom-out
     */
    void zoom(float factor);
}
