Crowd Simulator GUI
=====================

Uses [crowd simulator engine](https://gitlab.com/alonano/simulators/crowd/sim-engine). 

How to get the executable
--------------------------

Look at artifacts for the master branch build. The Jar file inside the
archive is a fat jar containing everything. Run it with:

    java -jar <name of the jar from the artifact archive>
    
